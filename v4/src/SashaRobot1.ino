int motorPinLeft = A3; 
int motorPinLeftBack = A4;

int motorPinRight = A1; 
int motorPinRightBack = A2; 


int echoPin = 2; 
int  trigPin = 3; 

void setup() {
  Serial.begin(9600);
  Serial.write("init");
  // put your setup code here, to run once:
  pinMode(motorPinLeft, OUTPUT);
  pinMode(motorPinRight, OUTPUT);
  pinMode(motorPinLeftBack, OUTPUT);
  pinMode(motorPinRightBack, OUTPUT);
  pinMode(trigPin, OUTPUT); 
  pinMode(echoPin, INPUT); 

  delay(1000);    
  Go();
  Serial.write("init end");
 }

void Go(){
  Left_Go();
  Right_Go();
}
void Left(){
  Left_Go();
  Right_Back();
}
void Right(){
  Left_Back();
  Right_Go();
}
void Back(){
  Left_Back();
  Right_Back();
}
void Stop(){
  Right_Stop();
  Left_Stop();
}

void Left_Stop(){
   digitalWrite(motorPinLeftBack, LOW);
   digitalWrite(motorPinLeft, LOW);
}
void Left_Back(){
   digitalWrite(motorPinLeftBack, HIGH);
   digitalWrite(motorPinLeft, LOW);
}
void Left_Go(){
   digitalWrite(motorPinLeftBack, LOW);
   digitalWrite(motorPinLeft, HIGH);
}

void Right_Stop(){
  digitalWrite(motorPinRightBack,LOW);
  digitalWrite(motorPinRight, LOW );
}
void Right_Back(){
  digitalWrite(motorPinRightBack,HIGH);
  digitalWrite(motorPinRight, LOW );
}
void Right_Go(){
  digitalWrite(motorPinRightBack, LOW);
  digitalWrite(motorPinRight, HIGH);
}

int GetDistance(){
  digitalWrite(trigPin, LOW); 
  delayMicroseconds(2); 
  digitalWrite(trigPin, HIGH); 
  delayMicroseconds(10); 
  digitalWrite(trigPin, LOW); 
  int duration = pulseIn(echoPin, HIGH); 
  return duration / 5.8;
}

void checkDistance(){
  int res = GetDistance();
  Serial.println(res);
  if(res<200&&res>0){
     Back();
     delay(200); 
     Right();
     delay(200); 
     Go();
    }
}

void loop() {
  checkDistance();
  delay(20);
  Serial.write("loop");
  
  // put your main code here, to run repeatedly:
  /*Left();
  delay(1000);    
  Right();
  delay(1000);   
  Go();
  delay(1000); 
  Back(); 
  delay(1000); */
}
