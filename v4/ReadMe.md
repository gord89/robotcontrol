#

Install:
```
sudo apt-get install  dnsmasq dkms hostapd -y
sudo apt-get install apache2 python-pip nano motion samba python-pip apache2 libapache2-mod-wsgi v4l-utils ser2net
sudo a2dismod mpm_event #(ust register Python with Apache)
sudo a2enmod mpm_prefork cgi

sudo apt-get install python-serial

```


nano /etc/network/interfaces
```
source /etc/network/interfaces.d/*
# Network is managed by Network manager
auto lo
iface lo inet loopback


auto eth0
iface eth0 inet static
address 192.168.200.10
netmask 255.255.255.0
gateway 192.168.200.1

auto wlan0
iface wlan0 inet static
address 192.168.12.1
netmask 255.255.255.0
network 192.168.12.0
```



Install create_ap
```
git clone https://github.com/oblique/create_ap
cd create_ap
sudo make install
```

sudo nano /etc/create_ap.conf
```
CHANNEL=4
GATEWAY=192.168.12.1
WPA_VERSION=1+2
ETC_HOSTS=0
DHCP_DNS=gateway
NO_DNS=0
HIDDEN=0
MAC_FILTER=0
MAC_FILTER_ACCEPT=/etc/hostapd/hostapd.accept
ISOLATE_CLIENTS=0
SHARE_METHOD=nat
IEEE80211N=0
IEEE80211AC=0
HT_CAPAB=’[HT40+]’
VHT_CAPAB=
DRIVER=nl80211
NO_VIRT=0
COUNTRY=
FREQ_BAND=2.4
NEW_MACADDR=
DAEMONIZE=0
NO_HAVEGED=0
WIFI_IFACE=wlan0
INTERNET_IFACE=eth0
SSID=robot_control
PASSPHRASE=orangepi
USE_PSK=0
```


Init service
```
systemctl daemon-reload
systemctl start create_ap.service
systemctl status create_ap.service
```


##smb config, nano /etc/samba/smb.conf 
/*[Data]
comment = home folder
path = /media/data
browseable = yes
writeable = yes
guest ok = yes
create mask = 0777
directory mask = 0777
force user = root
*/

[WWW]
comment = WWW
path = /var/www
browseable = yes
writeable = yes
guest ok = yes
create mask = 0777
directory mask = 0777
force user = root

[Motion]
comment = Motion
path = /etc/motion
browseable = yes
writeable = yes
guest ok = yes
create mask = 0777
directory mask = 0777
force user = root


## ser2net
sudo nano /etc/ser2net.conf
```
4000:telnet:600:/dev/ttyS0:115200 8DATABITS NONE 1STOPBIT
```
  
##Web app add python
sudo nano /etc/apache2/sites-enabled/000-default.conf
```
<Directory /var/www/html>
		Options +ExecCGI
		DirectoryIndex index.html
</Directory>
AddHandler cgi-script .py
```

```
sudo chmod -R 777 /var/www/html
```


##fix permision
http://techqa.info/programming/question/27858041/oserror:-[errno-13]-permission-denied:-%27/dev/ttyacm0%27---using-pyserial-from-python-to-arduino
# navigate to rules.d directory
cd /etc/udev/rules.d
#create a new rule file
sudo touch my-newrule.rules
# open the file
sudo vim my-newrule.rules
# add the following
KERNEL=="ttyS0", MODE="0666"



##test webcam:
```
v4l2-ctl --list-formats-ext
``




##Update file rc.local:
sudo nano /etc/rc.local
```
sudo motion

```