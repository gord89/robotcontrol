#include <Servo.h> 
#include <MyServo.h> 
#define SER_BAUD  115200
//#define SER_BAUD  9600


MyServo servoLaserH;
MyServo servoLaserV;
MyServo servoCameraV;
MyServo servoHeadH;

//servo
int servoPinLaserH =  6;
int servoPinLaserV =  7;
int servoPinCameraV =  9;
int servoPinHeadH =  8;
//motors
int motorPinLeft = 11; 
int motorPinLeftBack = 10;
int motorPinRight = 13; 
int motorPinRightBack = 12; 


//stops
int stopForwardLeft=A2;
int stopForwardRight=A3;
int stopBackLeft=A1;
int stopBackRight=A0;

int echoPin = 2; 
int  trigPin = 3; 

int laserPin =  5;
int lightPin =  4;

int pos1 = 0; 
int pos2 = 0; 

//int motorLeft = 12; 
//int motorRight = 13; 

int a = 0;
int i = 0;
int serIn; // var that will hold the bytes-in read from the serialBuffer
String serInString; // array that will hold the different bytes 100=100characters;
                        // -> you must state how long the array will be else it won't work.
int serInIndx = 0; // index of serInString[] in which to insert the next incoming byte
int serOutIndx = 0;

void setup() 
{ 
  //Serial.begin(9600);
  Serial.begin(SER_BAUD);
  pinMode(13, OUTPUT);
  
  pinMode(laserPin, OUTPUT);     
  pinMode(lightPin, OUTPUT);     
  
  //set motor
  pinMode(motorPinLeft, OUTPUT);
  pinMode(motorPinRight, OUTPUT);
  pinMode(motorPinLeftBack, OUTPUT);
  pinMode(motorPinRightBack, OUTPUT);

 //set stops 
  pinMode(stopForwardLeft, INPUT);
  pinMode(stopForwardRight, INPUT);
  pinMode(stopBackLeft, INPUT);
  pinMode(stopBackRight, INPUT);


 attachInterrupt(stopForwardLeft, OnStop, CHANGE);
 attachInterrupt(stopForwardRight, OnStop, CHANGE);
 attachInterrupt(stopBackLeft, OnStop, CHANGE);
 attachInterrupt(stopBackRight, OnStop, CHANGE);



  pinMode(trigPin, OUTPUT); 
  pinMode(echoPin, INPUT); 
  
  //myservo1.attach(servoPin1);
 // myservo2.attach(servoPin2);
  
  //myservo1.write(10);
  //myservo2.write(0);
  //delay(1000);     
  //myservo1.write(160);
  //myservo2.write(160);
  //servo3.Set(120);
  
  servoHeadH= MyServo(servoPinHeadH,10,160,90);
  delay(500); 
  //servoHead.Set(90);
  
  servoCameraV= MyServo(servoPinCameraV,20,150,70);
  delay(500); 
  //servoCameraV.Set(160);
  
  servoLaserH= MyServo(servoPinLaserH,30,140,90);
  delay(500); 
  //servoLaserH.Set(160);
  
  servoLaserV= MyServo(servoPinLaserV,30,140,30);
  delay(500); 
  //servoLaserV.Set(160);
  
  //servoHead.Set(90);
  //servoCameraV.Set(160);
  //servoLaserH.Set(160);
  //servoLaserV.Set(160);
  
//servoHeadH.Def();
  //servoCameraV.Def();
  //servoCameraV.Set(30);
  //servoLaserH.Def();
  //servoLaserV.Def();
  
  //digitalWrite(laserPin, HIGH);   // turn the LED on (HIGH is the voltage level)
  //delay(1000);               // wait for a second
  //digitalWrite(laserPin, LOW);    // turn the LED off by making the voltage LOW
  //delay(1000);
  //digitalWrite(laserPin, HIGH);
  
   /*  motor('r', 3);
     motor('l', 3);
     delay(500); 
     motor('r', 1);
     motor('l', 1);
     delay(500); 
     motor('r', 2);
     motor('l', 2);*/
} 

void loop() {
  checkDistance();
  
  if(Serial.available()) 
  {
    readSerialString();
     if (serInString[0]=='l'||serInString[0]=='L'){
         if(serInString[0]=='l'){
           if(serInString[1]=='1')
             digitalWrite(lightPin, HIGH);
           else
             digitalWrite(lightPin, LOW);
         }
        if(serInString[0]=='L'){
           if(serInString[1]=='1')
             digitalWrite(laserPin, HIGH);
           else
             digitalWrite(laserPin, LOW);
         }
     }
    
     if (serInString[0]=='s')
      {
      
    
        char lServo= serInString[1];
        //int pos = My_satoi(cutString(serInString,2));
       

        if (lServo=='1'){
          //Serial.println('!');
          //Serial.println(serInString[2]);
          servoHeadH.Set2(serInString[2]);
        }
        if (lServo=='2'){
           Serial.println('!');
          Serial.println(serInString[2]);
          servoCameraV.Set2(serInString[2]);
        }
        if (lServo=='3'){
           //Serial.println('!');
          //Serial.println(serInString[2]);
          servoLaserH.Set2(serInString[2]);
        }
        if (lServo=='4'){
          //Serial.println('!');
          //Serial.println(serInString[2]);
          servoLaserV.Set2(serInString[2]);
        }
      }
      if (serInString[0]=='m')
      {
        //Serial.println(serInString);
        motor('l', serInString[1]);
        motor('r', serInString[2]);
      }
  }
  delay(20);
  /*servoLaserH.Set(22);
  delay(500);
  servoLaserV.Set(130);
  delay(1000);
  
  
  
  servoLaserH.Set(120);
  delay(500);
 
   
  servoLaserV.Set(100);
  delay(1000);
  
  servoLaserH.Set(23);
  delay(500);
  servoLaserH.Set(120);
  delay(500);
  */
   //digitalWrite(laserPin, LOW);
  
}

void motor(char motorId, char actChar)
{
  int act=0;
  if(actChar=='1')
    act=1;
  if(actChar=='2')
    act=2;
  if(actChar=='3')
    act=3;
  motor(motorId,act);
}

void motor(char motorId, int act)
{
  int tmp_motor=0;
  int tmp_motorBack=0;
  if (motorId=='l')
  {
    tmp_motor=motorPinLeft;
    tmp_motorBack=motorPinLeftBack;
  }
  if (motorId=='r')
  {
    tmp_motor=motorPinRight;
    tmp_motorBack=motorPinRightBack;
  }
  
   if (act==1)
    {
     digitalWrite(tmp_motor, LOW);
     digitalWrite(tmp_motorBack, HIGH);
   }
       
   if (act==2)
   {
    digitalWrite(tmp_motor, LOW);
    digitalWrite(tmp_motorBack, LOW);
  }
       
  if (act==3)
  {
    digitalWrite(tmp_motor, HIGH);
    digitalWrite(tmp_motorBack, LOW);
  }
}
void readSerialString () {
  serInString="";
   while (Serial.available()&&serInString.length()<3) {
    delay(2);  //delay to allow byte to arrive in input buffer
    char c = Serial.read();
    serInString += c;
  }
}
 int My_satoi( char *str ){  
  int r = 0;  
  int len = strlen(str);  
  for(int i=0; i<len; i++){  
   //Check if this is a number  
   if ( str[i] < 0x3a && str[i] > 0x2f){  
     // is a ASCII number, return it  
     r = r * 10;  
   r += (str[i]-0x30);  
   }else{  
    i = len; //exit!  
    r = -1;  
    break;  
   }    
  }  
  return r;   
 } 

char* cutString(char* input,int start)
{
  char res[200];
  for (int i=start;i<serInIndx;i++)
    res[i-start]=input[i];
  res[serInIndx-start]='\0';
  return res;
}

void OnStop(){
  digitalWrite(13, HIGH);   // turn the LED on (HIGH is the voltage level)
  motor('r', 2);
  motor('l', 2);
  delay(1000);              // wait for a second
  digitalWrite(13, LOW);    // turn the LED off by making the voltage LOW
}

int GetDistance(){
  digitalWrite(trigPin, LOW); 
  delayMicroseconds(2); 
  digitalWrite(trigPin, HIGH); 
  delayMicroseconds(10); 
  digitalWrite(trigPin, LOW); 
  int duration = pulseIn(echoPin, HIGH); 
  return duration / 5.8;
}

void checkDistance(){
  int res = GetDistance();
  //Serial.println(res);
  if(res<80&&res>0){
     motor('r', 1);
     motor('l', 1);
     delay(500); 
     motor('r', 2);
     motor('l', 2);
    }
}
