package robo;

import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;

import android.util.Log;

public class net {

  public static String ip="";
  public static int port=2000;
	
  
   public static void sendData2(final String data)
	    {
	    	Thread background = new Thread(new Runnable() {
	            public void run() {
	            	try
	            	{
	            		Socket s = new Socket(ip,port);
	            	    OutputStream out = s.getOutputStream();
	            	    PrintWriter output = new PrintWriter(out);
	            	    output.print(data);
	            	    output.close();
	            	}
	                catch (Exception ex){
	                    Log.e("Debug", "error: " + ex.getMessage(), ex);
	                }
	            }
	    	 });
	    	 background.start();
	    }
	    
	 public static void sendData(final String data)
	    {
	    	Thread background = new Thread(new Runnable() {
	            public void run() {
	            	try
	            	{
	            		Socket s = new Socket(ip,port);
	            		//outgoing stream redirect to socket
	            	    OutputStream out = s.getOutputStream();
	            	       
	            	    PrintWriter output = new PrintWriter(out);
	            	    output.println(data);
	            	    output.close();
	            	  }
	                catch (Exception ex){
	                    Log.e("Debug", "error: " + ex.getMessage(), ex);
	                }
	            }
	    	 });
	    	 background.start();
	    }
}
