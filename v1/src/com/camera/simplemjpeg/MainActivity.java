package com.camera.simplemjpeg;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URI;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import robo.*;
import android.app.Activity;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.ToggleButton;

import com.multytuch.MultiTouchActivity;
import com.customProgressBar.VerticalSeekBar;

import java.net.Socket;
import java.util.Scanner;


public class MainActivity extends MultiTouchActivity {
	
	 Scanner scanner = new Scanner(System.in);
	private static final boolean DEBUG=false;
    private static final String TAG = "MJPEG";

    private MjpegView mv = null;
    String URL;
    
    // for settings (network and resolution)
    private static final int REQUEST_SETTINGS = 0;
	private static final String DEBUG_TAG = "HttpExample";
    
    private int width = 640;
    private int height = 480;
    
    private int ip_ad1 = 192;
    private int ip_ad2 = 168;
    private int ip_ad3 = 217;
    private int ip_ad4 = 1;
    private int ip_port = 8080;
    private String ip_command = "?action=stream";
    
    private boolean suspending = false;
 
    public char OldValueL ='2';
    public char OldValueR ='2';
    public String ipNet ="";
    
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences preferences = getSharedPreferences("SAVED_VALUES", MODE_PRIVATE);
        width = preferences.getInt("width", width);
        height = preferences.getInt("height", height);
        ip_ad1 = preferences.getInt("ip_ad1", ip_ad1);
        ip_ad2 = preferences.getInt("ip_ad2", ip_ad2);
        ip_ad3 = preferences.getInt("ip_ad3", ip_ad3);
        ip_ad4 = preferences.getInt("ip_ad4", ip_ad4);
        ip_port = preferences.getInt("ip_port", ip_port);
        ip_command = preferences.getString("ip_command", ip_command);
                
        StringBuilder sb = new StringBuilder();
        String s_http = "http://";
        String s_dot = ".";
        String s_colon = ":";
        String s_slash = "/";
        sb.append(s_http);
        sb.append(ip_ad1);
        sb.append(s_dot);
        sb.append(ip_ad2);
        sb.append(s_dot);
        sb.append(ip_ad3);
        sb.append(s_dot);
        sb.append(ip_ad4);
        sb.append(s_colon);
        sb.append(ip_port);
        sb.append(s_slash);
        sb.append(ip_command);
        URL = new String(sb);
     	//String URL="http://192.168.217.1:8080/?action=stream";
        String URL = sb+ip_command;
        robo.net.ip = ip_ad1 + s_dot+ip_ad2 + s_dot+ip_ad3 + s_dot+ip_ad4;
        
     //	width = 320;
   //  	height=240;
        setContentView(R.layout.main);
        mv = (MjpegView) findViewById(R.id.mv);  
        if(mv != null){
        	mv.setResolution(width, height);
        }
        new DoRead().execute(URL);
        
        
        final VerticalSeekBar vSeekBar = (VerticalSeekBar)findViewById(R.id.SeekBar01);
        final VerticalSeekBar vSeekBar2 = (VerticalSeekBar)findViewById(R.id.SeekBar02);
        
        final SeekBar vSeekBarH = (SeekBar)findViewById(R.id.seekBarH);
        final VerticalSeekBar vSeekBarV = (VerticalSeekBar)findViewById(R.id.SeekBarV);
        
        vSeekBar.setOnTouchListener(this);//left motor
        vSeekBar2.setOnTouchListener(this);//right motor
        
        //servo drive vertically
        vSeekBarH.setOnSeekBarChangeListener( new OnSeekBarChangeListener()
        {
         public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
           {
        	 if(fromUser)
        	 {
        		  net.sendData2("s2"+(180-progress));
        	 }
            }
	        public void onStartTrackingTouch(SeekBar seekBar)
	        {
	        }
	         public void onStopTrackingTouch(SeekBar seekBar)
	        {
	         }
        });
        
        //servo drive horizontal
        vSeekBarV.setOnSeekBarChangeListener(new VerticalSeekBar.OnSeekBarChangeListener() {
        	@Override
    		public void onStopTrackingTouch(VerticalSeekBar seekBar) {
    		}

    		@Override
    		public void onStartTrackingTouch(VerticalSeekBar seekBar) {
    		}

    		@Override
    		public void onProgressChanged(VerticalSeekBar seekBar, int progress,
    				boolean fromUser) {
    		//	if(fromUser)
	           	 {
	           		 net.sendData2("s1"+progress);
	           	 }
    		}
    	});
        
        
        //left motor
        vSeekBar.setOnSeekBarChangeListener(new VerticalSeekBar.OnSeekBarChangeListener() {
        	//@Override void 
    		@Override
    		public void onStopTrackingTouch(VerticalSeekBar seekBar) {
        		seekBar.setProgress(50);
    		}

    		@Override
    		public void onStartTrackingTouch(VerticalSeekBar seekBar) {
    		}

    		@Override
    		public void onProgressChanged(VerticalSeekBar seekBar, int progress,
    				boolean fromUser) {
    		//	if (fromUser)
    				Compare(progress,vSeekBar2.getProgress());
    		}
    	});
        
        
        //right motor
		vSeekBar2.setOnSeekBarChangeListener(new VerticalSeekBar.OnSeekBarChangeListener() {
        	@Override
    		public void onStopTrackingTouch(VerticalSeekBar seekBar) {
        		seekBar.setProgress(50);
    		}

    		@Override
    		public void onStartTrackingTouch(VerticalSeekBar seekBar) {
    		}

    		@Override
    		public void onProgressChanged(VerticalSeekBar seekBar, int progress,
    				boolean fromUser) {
    			///if (fromUser)
    				Compare(vSeekBar.getProgress(),progress);
    		}
    	});
    }

    
    public void Compare(int prL,int prR )
    {
    	char curentL =control.getCommand(prL);
    	char curentR =control.getCommand(prR);
    	if(curentL!=OldValueL || curentR!=OldValueR)
    	{
    		OldValueL =curentL;
    		OldValueR =curentR;    		
    		net.sendData("m"+ curentR+curentL);
    	}
    }
    
    public void onToggleClicked(View view) {
        boolean on = ((ToggleButton) view).isChecked();
        if (on) {
        	net.sendData("l1");
        } else {
        	net.sendData("l0");
        }
    }
    
 
   public void Clicked_bt_stop(View v) {
	   //stop all motors
    	net.sendData("m22");
   }
    public void Clicked_bt_go(View v) {
 	   //starting all motors
    	net.sendData("m33");
   }
    
    public void Clicked_bt_clear(View v) {
    	net.sendData2("s190");
    	net.sendData2("s290");
   }

    
    public void onResume() {
    	if(DEBUG) Log.d(TAG,"onResume()");
        super.onResume();
        if(mv!=null){
        	if(suspending){
        		new DoRead().execute(URL);
        		suspending = false;
        	}
        }

    }

    public void onStart() {
    	if(DEBUG) Log.d(TAG,"onStart()");
        super.onStart();
    }
    public void onPause() {
    	if(DEBUG) Log.d(TAG,"onPause()");
        super.onPause();
        if(mv!=null){
        	if(mv.isStreaming()){
		        mv.stopPlayback();
		        suspending = true;
        	}
        }
    }
    public void onStop() {
    	if(DEBUG) Log.d(TAG,"onStop()");
        super.onStop();
    }

    public void onDestroy() {
    	if(DEBUG) Log.d(TAG,"onDestroy()");
    	
    	if(mv!=null){
    		mv.freeCameraMemory();
    	}
    	
        super.onDestroy();
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	MenuInflater inflater = getMenuInflater();
    	inflater.inflate(R.layout.option_menu, menu);
    	return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	switch (item.getItemId()) {
    		case R.id.settings:
    			Intent settings_intent = new Intent(MainActivity.this, SettingsActivity.class);
    			settings_intent.putExtra("width", width);
    			settings_intent.putExtra("height", height);
    			settings_intent.putExtra("ip_ad1", ip_ad1);
    			settings_intent.putExtra("ip_ad2", ip_ad2);
    			settings_intent.putExtra("ip_ad3", ip_ad3);
    			settings_intent.putExtra("ip_ad4", ip_ad4);
    			settings_intent.putExtra("ip_port", ip_port);
    			settings_intent.putExtra("ip_command", ip_command);
    			startActivityForResult(settings_intent, REQUEST_SETTINGS);
    			return true;
    	}
    	return false;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
    	switch (requestCode) {
    		case REQUEST_SETTINGS:
    			if (resultCode == Activity.RESULT_OK) {
    				width = data.getIntExtra("width", width);
    				height = data.getIntExtra("height", height);
    				ip_ad1 = data.getIntExtra("ip_ad1", ip_ad1);
    				ip_ad2 = data.getIntExtra("ip_ad2", ip_ad2);
    				ip_ad3 = data.getIntExtra("ip_ad3", ip_ad3);
    				ip_ad4 = data.getIntExtra("ip_ad4", ip_ad4);
    				ip_port = data.getIntExtra("ip_port", ip_port);
    				ip_command = data.getStringExtra("ip_command");

    				if(mv!=null){
    					mv.setResolution(width, height);
    				}
    				SharedPreferences preferences = getSharedPreferences("SAVED_VALUES", MODE_PRIVATE);
    				SharedPreferences.Editor editor = preferences.edit();
    				editor.putInt("width", width);
    				editor.putInt("height", height);
    				editor.putInt("ip_ad1", ip_ad1);
    				editor.putInt("ip_ad2", ip_ad2);
    				editor.putInt("ip_ad3", ip_ad3);
    				editor.putInt("ip_ad4", ip_ad4);
    				editor.putInt("ip_port", ip_port);
    				editor.putString("ip_command", ip_command);

    				editor.commit();

    				new RestartApp().execute();
    			}
    			break;
    	}
    }

    public class DoRead extends AsyncTask<String, Void, MjpegInputStream> {
        protected MjpegInputStream doInBackground(String... url) {
            //TODO: if camera has authentication deal with it and don't just not work
            HttpResponse res = null;
            DefaultHttpClient httpclient = new DefaultHttpClient(); 
            HttpParams httpParams = httpclient.getParams();
            HttpConnectionParams.setConnectionTimeout(httpParams, 5*1000);
            Log.d(TAG, "1. Sending http request");
            try {
                res = httpclient.execute(new HttpGet(URI.create(url[0])));
                Log.d(TAG, "2. Request finished, status = " + res.getStatusLine().getStatusCode());
                if(res.getStatusLine().getStatusCode()==401){
                    //You must turn off camera User Access Control before this will work
                    return null;
                }
                return new MjpegInputStream(res.getEntity().getContent());  
            } catch (ClientProtocolException e) {
                e.printStackTrace();
                Log.d(TAG, "Request failed-ClientProtocolException", e);
                //Error connecting to camera
            } catch (IOException e) {
                e.printStackTrace();
                Log.d(TAG, "Request failed-IOException", e);
                //Error connecting to camera
            }
            return null;
        }

        protected void onPostExecute(MjpegInputStream result) {
            mv.setSource(result);
            if(result!=null) result.setSkip(1);
            mv.setDisplayMode(MjpegView.SIZE_BEST_FIT);
            mv.showFps(false);
        }
    }
    
    public class RestartApp extends AsyncTask<Void, Void, Void> {
        protected Void doInBackground(Void... v) {
			MainActivity.this.finish();
            return null;
        }

        protected void onPostExecute(Void v) {
        	startActivity((new Intent(MainActivity.this, MainActivity.class)));
        }
    }
}
